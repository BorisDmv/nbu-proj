using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ZombieController : MonoBehaviour
{
    public GameObject player;
    //PlayerHealth playerHealth;
    ZombieHealth zombieHealth;
    UnityEngine.AI.NavMeshAgent zombieAi;

    void Awake(){
        //playerHealth = player.GetComponent <PlayerHealth> ();
        zombieHealth = GetComponent <ZombieHealth> ();
        zombieAi = GetComponent <UnityEngine.AI.NavMeshAgent> ();
     }

     void Update(){
         // && playerHealth.currentHealth > 0
         if(zombieHealth.currentHealth > 0 )
        {
            zombieAi.SetDestination (player.transform.position);
        }
        else
        {
            zombieAi.enabled = false;
        }
     }
}
